import argparse
from sys import exit, stderr, argv


def commands(controller):
    parser = argparse.ArgumentParser(
        description="Add a description for the programm here"
    )
    parser.add_argument(
        "--zk-dir",
        type=str,
        nargs=1,
        help="The directory for zk. If not set $ZK_NOTEBOOK_DIR will be used",
    )

    subparsers = parser.add_subparsers(
        title="Subcommands", description="Search for help with SUBCOMMAND --help"
    )

    parser_version = subparsers.add_parser(
        "version", help="Prints out the Version of the Programm"
    )
    parser_version.set_defaults(func=controller.command_version)

    parser_convert_links = subparsers.add_parser(
        "convert_links", help="Converts link formats"
    )
    parser_convert_links.add_argument("-p", "--path", nargs=1, type=str)
    parser_convert_links.set_defaults(func=controller.command_convert_links)

    # prints help instead an exception when no arguments are given
    if len(argv) == 1:
        parser.print_help(stderr)
        exit(1)

    args = parser.parse_args()

    return args
