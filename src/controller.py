import os
from src.commands import commands
from src.config import config
from sys import exit, stderr

from src.correct_links import correct_links
from src.zk_wrapper import zk_list_notes

APP_NAME = "zk-utils"
APP_STAGE = "dev"

if os.name == "posix":
    DATA_PATH = os.path.join(
        os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.local/share")), APP_NAME
    )
    CONFIG_PATH = os.path.join(
        os.getenv("XDG_CONFIG_HOME", os.path.expanduser("~/.config")), APP_NAME
    )
else:
    raise Exception("Not supported OS, only POSIX currently")


class Controller:
    def __init__(self):
        self.config_default = self.load_config()
        self.init_directories()

    def init_directories(self):
        if not os.path.exists(DATA_PATH):
            os.mkdir(DATA_PATH)
        if not os.path.exists(CONFIG_PATH):
            os.mkdir(CONFIG_PATH)

    def read_arguments(self):
        args = commands(self)
        args.func(args)

    def load_config(self):
        return config()

    def parse_default_args(self, args):
        # Set zk_dir
        if args.zk_dir:
            self.zk_dir = os.path.expanduser(args.zk_dir[0])
        elif "ZK_NOTEBOOK_DIR" in os.environ:
            self.zk_dir = os.getenv("ZK_NOTEBOOK_DIR")
        else:
            print(
                "No zk folder found. Either set $ZK_NOTEBOOK_DIR or set manually with --zk-dir",
                stderr,
            )
            exit(1)
        

    def command_convert_links(self, args):
        self.parse_default_args(args)
        if args.path:
            zk_subdir = args.path[0]
        else:
            zk_subdir = ""
        notes_list = zk_list_notes(self.zk_dir, zk_subdir)
        # print(notes_list)
        corrected_links_counts = []
        for current_note in notes_list:
            note_abs_path = os.path.join(self.zk_dir, current_note)
            corrected_links_counts.append(correct_links(note_abs_path, self.zk_dir))
        print(f"Corrected {sum(corrected_links_counts)} links!")

    def command_version(self):
        print("Version: Dev")
