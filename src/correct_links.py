import re
import os
from glob import glob
from src.zk_wrapper import zk_get_note_title_from_path


def get_full_note_path(zk_notebook_dir: str, note_link: str) -> str:
    """
    The [[WikiLinks]] Syntax does not precisely reflect the path of the file.
    This function looks for actual existence of a file and therefore for
    existance for the note.

    When nothing is found this function returns an empty string.
    """
    # Append file extension if not present
    if not note_link.endswith(".md"):
        note_link = f"{note_link}.md"

    # Simple Check
    full_path = os.path.join(zk_notebook_dir, f"{note_link}")
    if os.path.exists(full_path):
        return full_path

    # Find with glob
    glob_paths = glob(str(zk_notebook_dir) + f"/**/{note_link}")
    if len(glob_paths) == 1:
        return glob_paths[0]

    # Return empty string when nothing is found
    return ""


def correct_links(current_note_abs_path, zk_notebook_dir) -> int:
    """
    1. parse link into following fields from [[WikiLink]] Syntax:
    2. build new Markdown Link
    3. Replace [[WikiLink]] with Markdown link
    """
    count = 0
    # print(f"Read {current_note_abs_path}") # DEBUG
    # read the note
    with open(current_note_abs_path, "r") as note:
        note_text = note.read()

    # find all WikiLinks with the syntax [[abcd|name of the link]]
    matches = re.findall(f"\[\[[a-z].*?\]\]", note_text)

    for match in matches:
        cropped = re.sub("\[|\]", "", match)
        link_parts = cropped.split("|")

        # create link destination
        link_dest = link_parts[0]
        link_dest_abs_path = get_full_note_path(zk_notebook_dir, link_dest)
        if link_dest_abs_path == "":
            output = f"{current_note_abs_path} "
            output += f'"{zk_get_note_title_from_path(current_note_abs_path)}"'
            output += f": Can't resolve {match}!"
            print(output)
            # Skip this match and do not modify any text
            continue
        link_src_split = os.path.split(current_note_abs_path)
        link_dest_split = os.path.split(link_dest_abs_path)

        rel_dest_link = ""
        # print(link_src_split, link_dest_split)   # DEBUG
        if not link_src_split[0] == link_dest_split[0]:
            rel_dest_link = os.path.relpath(link_dest_split[0], link_src_split[0])

        rel_dest_link = os.path.join(rel_dest_link, link_dest_split[1]).rpartition(".md")[0]

        # create link text
        link_text = ""
        if len(link_parts) > 1:
            link_text = link_parts[1]
        else:
            link_text = zk_get_note_title_from_path(link_dest_abs_path)

        # assemble text and link together to markdown link
        markdown_link_text = f"[{link_text}]({rel_dest_link})"

        count += 1

        # print("\n", f"{match}: {markdown_link_text}")

        note_text = note_text.replace(match, markdown_link_text)

    with open(current_note_abs_path, "w") as note_file:
        note_file.write(note_text)

    return count
