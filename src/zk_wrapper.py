import subprocess


def zk_list_notes(zk_notebook_dir, zk_arg_path, zk_arg_search_string=""):
    zk_output = subprocess.run(
        [
            'zk',
            f'--working-dir={zk_notebook_dir}',
            f'--notebook-dir={zk_notebook_dir}',
            'list',
            '--format={{path}}',
            zk_arg_path,
            zk_arg_search_string,
        ],
        text=True,
        capture_output=True,
    ).stdout.strip().split("\n")

    return zk_output


def zk_get_note_title_from_path(zk_note_path):
    zk_output = subprocess.run(
        ['zk', 'list', zk_note_path, '--format={{title}}', '--quiet'],
        text=True,
        capture_output=True,
    ).stdout.strip()

    return zk_output
